---
menu-title: Place
weight: 40
---
#
# PLACE
Festival takes place in a beautiful landscape of Daugavas Loki nature park. There is Daugava River nearby (15 minutes walk). 
Magnificient hills, lively swamp, birch grove, ancient stone buildings covered with ivy await you, fellow Shayaner. 
There are no shops close by, so make sure you are stocked with food and supplies, including water! We will provide water on-site, but our supply is limited.


## How to get there?


* Shuttle transfer from Riga is available, from aiport and central bus station (Rigas Autoosta).
If you want to book festival shuttle, you can easily do it with our partner
    [latvia-outdoor.de](http://latvia-outdoor.de/shayanaland.htm)
        

    
* Public transport access is a limited in the area. Bus goes from Daugavpils Bus Station (Bus to Mālkalni). Please ASK driver to stop at Orehovka bus stop, it is by-demand only.

    [Bus Schedule](https://www.1188.lv/en/transport/schedules/daugavpils-ao/malkalni/105258/103409/day/2018-05-25)
    
        

* Even better is to use a car by cooperating with other visitors! Share your ride in our
 
    [faceboook event](https://www.facebook.com/events/2030192027306568/)
    
        

* Exact coordinates are following: **26°50'57.10"E 55°52'33.10"N**  or  **55.875893, 26.849181**

    [Google Maps route](https://www.google.lv/maps/dir/%D0%94%D0%B0%D1%83%D0%B3%D0%B0%D0%B2%D0%BF%D0%B8%D0%BB%D1%81,+Daugavpils+pils%C4%93ta/55.8758611,26.8491944/@55.8717293,26.6104813,20540m/data=!3m2!1e3!4b1!4m8!4m7!1m5!1m1!1s0x46c29430ff01c9db:0x400cfcd68f307f0!2m2!1d26.536179!2d55.874736!1m0)
    
    
    
<iframe src="https://www.google.com/maps/embed?pb=!1m26!1m12!1m3!1d112963.13184779328!2d26.54043883133212!3d55.871573320566974!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m11!3e6!4m5!1s0x46c29430ff01c9db%3A0x400cfcd68f307f0!2z0JTQsNGD0LPQsNCy0L_QuNC70YEsIERhdWdhdnBpbHMgcGlsc8STdGE!3m2!1d55.874736!2d26.536178999999997!4m3!3m2!1d55.875861099999995!2d26.8491944!5e0!3m2!1sen!2slv!4v1495179725545"  frameborder="0" style="width:80%;min-height:400px;border:0" allowfullscreen></iframe>

* Also, it is very easy to get there even without GPS, just few turns:

Drive away from Daugavpils, heading to Belarusian border, using road P68. Drive thru Griva district.

On first crossing after “End of Daugavpils city” road sign - turn left

[Pic1](/images/location/1.jpg "Pic1")

Drive this way for some time, pass big crossing with church straight. Soon the road will change from asphalted to country road, keep driving ahead

[Pic2](/images/location/2.jpg "Pic2")

Drive straight on big crossing of country roads 

[Pic3](/images/location/3.jpg "Pic3")

Soon you see the electricity line and pillar - this is a landmark of Shayanaland

[Pic4](/images/location/4.jpg "Pic4")

You are welcome!

The road is 28 kilometers long, counting from Daugavpils center; approximate driving time - about half an hour. Please, place your cars accurately on field, do not block traffic of cars and agricultural transport.
our.


## Where we can sleep?

Shayana is open air festival, and in May it can be quite breezy at night in Latgale! 
You can sleep and replenish your forces in the tent that you should bring by yourself. Please take everything necessary for comfortable 
sleepover in nature, most important being the sleeping bag.
If you are not used to spending the night in forest, you can rent good tourist equipment at our friends in [tourist club "Snowdrop"](http://www.sniegpulkstenite.lv/) (Daugavpils) 


## Isn't it too cold in May at night?
Statistics tell us that temperature rarely drops below +6 degrees Celsius. Make sure you have appropriate clothes. We will keep you warm using positive energy and kindness, but this is not always enough. There will be big campfires on the territory, where you can warm up and drink some hot aromatic tea or herbal infusions.

But even better is to use true Shayanic way to get warm - go the Energy Field stage and rock the dancefloor!

#### 