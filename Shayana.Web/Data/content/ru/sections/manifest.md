---
weight: 20
menu-title: Манифест
---
# 
# Манифест

Третий год Шаяны приходит, и возможности бесконечны!

Прошлый год был невероятно вдохновляющим, и мы продолжим расти и развиваться. 

Любители невероятного коктейля из хорошей музыки и дыхания ночного леса приглашаются! 


С нами отдыхают разные люди: и ценители вдумчивого слушания музыки у костра с чаем, и любители проводить всю ночь на танцполе. Все вы для нас важны! 


В 2018 году мы планируем вдохнуть новую жизнь в фестиваль, и изменить концепты сцен, чтобы подарить посетителям новое ощущение чуда. Мы будем раскрывать планы постепенно, так что оставайтесь с нами!

[Посол Шаяны в Facebook ](https://www.facebook.com/profile.php?id=100013224242512&fref=ts)–  наш друг из энергии и света. Он расскажет, как попасть на фестиваль и чем вы можете помочь.  

