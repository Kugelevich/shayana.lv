---

weight: 30
menu-title: FAQ
---
#
# FAQ


## What is the meaning of the festival name?

Shayana is Hindu female name that means "Gift of the gods". We think that the festival is our gift to ourselves. Making the party for everyone, we grow as people and bring a little bit of kindness to this planet. That's why your support and participation is so important.


## What is Shayana and when it came to being?

Shayana existed since the dawn of times. It never started and it will never end, we just haven't heard about it before. Now you know the truth!

## What about the booze?
We do not advice bringing alcohol to the festival, because its delicate and friendly atmosphere can suffer from this. If you still insist, please do not use glass bottles. Also, please don't start campfires by yourselves, we got this for you. Please, clean up after yourself.

Festival got magical guards. They are kind, but strict, and will ensure safety and comfort for everybody. Don't be afraid of them, they are here for your own security. Remember, we are all guests of Shayana and as guests should be respectful of local rules.

