function getFormData($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });
    
    return indexed_array;
}

function initTicketForm() {
	$.get("/content/"+lang+"/ticketForm.html").then(function (data)
    {
    	$('#ticketForm').html(data);
        updateTicketPrice();

        $('#TicketCount, #GroupTicket').change(updateTicketPrice);
    });
   
}

function updateTicketPrice() {
    var singleTicketPrice = 15;
    var groupTicketPrice = 50;

    var oneTicketPrice = $('#GroupTicket').prop('checked') ? groupTicketPrice : singleTicketPrice;

    var finalPrice = oneTicketPrice * $('#TicketCount').val();

    $('#ticketPrice').text(finalPrice);
}

var OrderTicket =
    function (args)
    {
    	var data=getFormData($('form'));
    	data['GroupTicket']=$('#GroupTicket').prop('checked')?"yes":"no";
        data['TicketCount']=$('#TicketCount').val();
        data['TicketPrice']=$('#ticketPrice').text();
		$.when(
        	$.post('https://hooks.zapier.com/hooks/catch/3164559/fhovn1/',data), 
			$.post('https://hooks.zapier.com/hooks/catch/3164559/fmxh2j/',data)
		)
			
            .then(function ()
            {                
                $('form').fadeOut('slow',function(){
                	$('.ticketOrderSuccess').fadeIn('slow');
				});                
                $('.ticketOrderError').hide();
            })
            .fail(function ()
            {
                $('.ticketOrderSuccess').hide();
                $('.ticketOrderError').hide().promise().then(
                	function(){$(this).fadeIn('slow');}
                 );            
                
            });
    };

var lang = "en";
function Common () {
    "use strict";
    
  
    
    hyperform(window);
    

    var route = location.pathname.split("/");
    if (route.length >= 2) {
        var routeLang = route[1];
        if (routeLang === "ru" || routeLang === "lv" || routeLang === "en") {
            lang = routeLang;
        }
    }

    //fancybox HACK
    setInterval(function () {
        $('.fancyWrap:visible img').each(function () {
            var src = $(this).attr('src');
            var dsrc = $(this).attr("data-src");
            if (src !== dsrc) {
                $(this).attr('src', dsrc);
            }
        }); }, 100);

    // if (!$.cookie("language")) {// do your stuff
    //
    //
    //     var language = navigator.languages && navigator.languages[0] ||
    //         navigator.language ||
    //         navigator.userLanguage;
    //     language = language.substring(0, 2);
    //     $.cookie("language", "language", { "expires": 30 });
    //
    //     if (lang !== language && (language === "lv" || language === "ru")) {
    //         window.location.assign("/" + language);
    //     }
    // }
    $('#lineup .active').click();
    $(".langSwitcher a:contains(" + lang.toUpperCase() + ")").addClass("active");
	//Hide Loading Box (Preloader)
	//function handlePreloader() {
	//	if($('.preloader').length){
	//		$('.preloader').delay(2000).fadeOut(2000);
	//	}
	//}
	// count down timer function
	function countdownTimer () {
		var countDownContainer = $('.count-down');
        if (countDownContainer.length) {
            var options = {
                date: "May 25, 2018 18:00"
            };
            if (lang === "ru" || lang === "lv") {
                options.render = function (date) {
                    var template;
                    if (lang === "ru") {
                        template = "<li> <span class='days'> <i>" +
                            date.days +
                            "</i>  Дней </span> </li>" +
                            "<li> <span class='hours'> <i>" +
                            date.hours +
                            "</i> Часов </span> </li>" +
                            "<li> <span class='minutes'> <i>" +
                            date.min +
                            "</i> Минут  </span> </li>" +
                            "<li> <span class='seconds'> <i>" +
                            date.sec +
                            "</i> Секунд </span> </li>";
                    } else {
                            template = "<li> <span class='days'> <i>" +
                            date.days +
                            "</i>  Dienas </span> </li>" +
                            "<li> <span class='hours'> <i>" +
                            date.hours +
                            "</i> Stundas </span> </li>" +
                            "<li> <span class='minutes'> <i>" +
                            date.min +
                            "</i> Minūtes </span> </li>" +
                            "<li> <span class='seconds'> <i>" +
                            date.sec +
                            "</i> Sekundes </span> </li>";
                    }

                    this.el.innerHTML = template;
                }
            }
            
            countDownContainer.countdown(options);

	    };
	}
	// sticky header 
	function stickyHeader () {
		var headerScrollPos = $('header').next().offset().top;
		if($(window).scrollTop() > headerScrollPos) {
			$('header').addClass('header-fixed gradient-overlay'); 
		}
		else if($(this).scrollTop() <= headerScrollPos) {
			$('header').removeClass('header-fixed gradient-overlay'); 
		}
	}
	function SmoothMenuScroll () {
		var anchor = $('.scrollToLink');
		if(anchor.length){
			anchor.children('a').bind('click', function (event) {
				var headerH = '95';
				var target = $(this);
				$('html, body').stop().animate({
					scrollTop: $(target.attr('href')).offset().top - headerH + 'px'
				}, 1200, 'easeInOutExpo');
				anchor.removeClass('current');
				target.parent().addClass('current');
				event.preventDefault();
			});
		}
	}
	// adding active class to menu while scroll to section
	function OnePageMenuScroll () {
	    var windscroll = $(window).scrollTop();
	    if (windscroll >= 100) {
	    	$('.mainmenu .scrollToLink').find('a').each(function (){
	    		// grabing section id dynamically
	    		var sections = $(this).attr('href');
		        $(sections).each(function() {
		        	// checking is scroll bar are in section
		            if ($(this).offset().top <= windscroll + 100) {
		            	// grabing the dynamic id of section
		        		var Sectionid = $(sections).attr('id');
		        		// removing current class from others
		        		$('.mainmenu').find('li').removeClass('current');
		        		// adding current class to related navigation
		        		$('.mainmenu').find('a[href=#'+Sectionid+']').parent().addClass('current');
		            }
		        });
	    	});
	    } else {
	        $('.mainmenu li.current').removeClass('current');
	        $('.mainmenu li:first').addClass('current');
	    }
	}
	// gallery fancybox activator 
    function GalleryFancyboxActivator () {
    	var galleryFcb = $('.fancybox');
    	if(galleryFcb.length){
    		galleryFcb.fancybox();
    	}
    }
    // upcoming event filter 
    function upcomingEventFilter () {
    	var upcomingEventFilterContent = $('#lineup .tab-content-wrap');
    	if (upcomingEventFilterContent) {
            upcomingEventFilterContent.mixItUp({
                load: {
                    filter: '.MF1'
                }
            });
    	};
    }
    // testimonial slider 
    function testimonialSlide () {
    	var sliderContainer = $('.testimonial-slide');
    	var customPager = $('.testimonial.custom-pager li[data-slide-index]');
    	if (sliderContainer.length) {
    		var slider = sliderContainer.bxSlider({
    			auto: true,
				autoControls: false,
				controls: false,
				pager: false,
				autoHover: true,
		    	minSlides: 1,
		    	onSlideNext: function () {
		    		var current = slider.getCurrentSlide();		    		
					customPager.each(function () {
						var Slef = $(this);
						var slideIndex = $(this).data('slide-index');
						if (slideIndex === current) {
							customPager.removeClass('active');
							Slef.addClass('active');
						}
					});
		    	}
			});
			customPager.each(function () {
				var slideIndex = $(this).data('slide-index');
				$(this).on('click', function () {
					customPager.removeClass('active');
					$(this).addClass('active');
					slider.goToSlide(slideIndex);
				});
			});
			$('#testimonials .custom-pager li.prev').on('click', function () {
			    var current = slider.getCurrentSlide();
			    slider.goToPrevSlide(current) - 1;
			});
			$('#testimonials .custom-pager li.next').on('click', function () {
			    var current = slider.getCurrentSlide();
			    slider.goToNextSlide(current) + 1;
			});
    	};
    }
    // sponsor logo carosule
    function sponsorLogo () {
    	var sponsorLogoContainer = $('.sponsor-logo');
    	if (sponsorLogoContainer.length) {
    		sponsorLogoContainer.owlCarousel({
			    loop: true,
			    margin: 100,
			    nav: true,
			    dots: false,
			    autoWidth: true,
	            navText: [
	                '<i class="fa fa-angle-left"></i>',
	                '<i class="fa fa-angle-right"></i>'
	            ],
	            autoplay:true,
			    autoplayTimeout:3000,
			    autoplayHoverPause:true,
			    responsive: {
			        0:{
			            items:1
			        },
			        480:{
			            items:2
			        },
			        600:{
			            items:3
			        },
			        1000:{
			            items:4
			        }
			    }
			});
    	}
    }
    
    function CounterNumberChanger () {
		var timer = $('.timer');
		if(timer.length) {
			timer.each(function () {
				$(this).appear(function () {
					var value = $(this).text();
					$(this).countTo({
						from: 1,
						to: value,
						speed: 3000
					});
				});
			});
		}

	}
	function expertizeRoundCircle () {
		var rounderContainer = $('.single-expertize .icon');
		if (rounderContainer.length) {
			rounderContainer.each(function () {
				var Self = $(this);
				var value = Self.data('value');
				var size = Self.parent().width();
				var color = Self.data('fg-color');

				Self.find('span').each(function () {
					var expertCount = $(this);
					expertCount.appear(function () {
						expertCount.countTo({
							from: 1,
							to: value*100,
							speed: 3000
						});
					});

				});
				Self.appear(function () {					
					Self.circleProgress({
						value: value,
						size: size,
						thickness: 20,
						emptyFill: 'rgba(0, 0, 0, .0)',
						animation: {
							duration: 3000
						},
						fill: {
							color: color
						}
					});
				});
			});
		};
	}
	function featureListTab () {
		var tabContent = $('.tab-row');
		if (tabContent.length) {
			tabContent.find('.tab-content-box').hide();
			tabContent.find('.tab-content-box').eq(0).show();
			tabContent.find('.tab-title li span').on('click', function () {
				tabContent.find('.tab-title li span').removeClass('active');
				$(this).addClass('active');
				var tabName = $(this).data('tab-name');
				tabContent.find('.tab-content-box').hide();
				tabContent.find('.tab-content-box.'+ tabName).fadeIn(500);
			});
		};
	}
	function DeadMenuConfig () {
		var deadLink = $('.mainmenu li.deadlink');
		if(deadLink.length) {
			deadLink.each(function () {
				$(this).children('a').on('click', function() {
					return false;
				});
			});
		}
	}
	// revolution slider 
	function revolutionSliderActiver () {
		var banner = $('#banner .banner');
		if (banner.length) {
			banner.revolution({
				delay:5000,
				startwidth:1170,
				startheight:820,
				startWithSlide:0,

				fullScreenAlignForce:"on",
				autoHeight:"off",
				minHeight:"off",

				shuffle:"off",

				onHoverStop:"on",


				hideThumbsOnMobile:"off",
				hideNavDelayOnMobile:1500,
				hideBulletsOnMobile:"off",
				hideArrowsOnMobile:"off",
				hideThumbsUnderResoluition:0,

				hideThumbs:1,
				hideTimerBar:"on",

				keyboardNavigation:"on",

				navigationType:"bullet",
				navigationArrows: "nexttobullets",
				navigationStyle:"preview4",

				navigationHAlign:"center",
				navigationVAlign:"bottom",
				navigationHOffset:30,
				navigationVOffset:30,

				soloArrowLeftHalign:"left",
				soloArrowLeftValign:"center",
				soloArrowLeftHOffset:20,
				soloArrowLeftVOffset:0,

				soloArrowRightHalign:"right",
				soloArrowRightValign:"center",
				soloArrowRightHOffset:20,
				soloArrowRightVOffset:0,


				touchenabled:"on",
				swipe_velocity:"0.7",
				swipe_max_touches:"1",
				swipe_min_touches:"1",
				drag_block_vertical:"false",

				parallax:"mouse",
				parallaxBgFreeze:"on",
				parallaxLevels:[10,7,4,3,2,5,4,3,2,1],
				parallaxDisableOnMobile:"off",

				stopAtSlide:-1,
				stopAfterLoops:-1,
				hideCaptionAtLimit:0,
				hideAllCaptionAtLilmit:0,
				hideSliderAtLimit:0,

				dottedOverlay:"none",

				spinned:"spinner4",

				fullWidth:"on",
				forceFullWidth:"on",
				fullScreen:"off",
				fullScreenOffsetContainer:"#banner",
				fullScreenOffset:"0px",

				panZoomDisableOnMobile:"off",

				simplifyAll:"off",

				shadow:0

			});
		};
	}
	// wow activator 
	function wowActivator () {
		var wow = new WOW ({
    		offset: 0
    	});
    	wow.init();
	}
	// mobile menu config
	function mobileMenuConfig () {
		var menuContainer = $('nav.mainmenu-container');
		if (menuContainer.length) {
			menuContainer.find('ul .dropdown').children('a').append(function () {
				return '<i class="fa fa-bars"></i>';
			});
			menuContainer.find('.fa').on('click', function () {
				$(this).parent().parent().children('ul').slideToggle(300);
				return false;
			});
			menuContainer.find('.nav-toggler').on('click', function () {
				$(this).parent().children('ul').slideToggle();
			});
			menuContainer.find('ul .nav-closer').on('click', function () {
				$(this).parent('ul').slideToggle();
			});
		};
	}
	//Contact Form Validation
	function contactFormValidation () {
		if($('.contact-form').length){
			$('.contact-form').validate({ // initialize the plugin
				rules: {
					name: {
						required: true
					},
					email: {
						required: true,
						email: true
					},
					message: {
						required: true
					},
					subject: {
						required: true
					}
				},
				submitHandler: function (form) { 
					// sending value with ajax request
					$.post($(form).attr('action'), $(form).serialize(), function (response) {
						$(form).parent('div').append(response);
						$(form).find('input[type="text"]').val('');
						$(form).find('input[type="email"]').val('');
						$(form).find('textarea').val('');
					});
					return false;
				}
			});
		}
	}

	// doc ready

		countdownTimer();
		SmoothMenuScroll();
		GalleryFancyboxActivator();
		upcomingEventFilter();
		testimonialSlide();
		sponsorLogo();
		//twitterFeedWidget();
		CounterNumberChanger();
		expertizeRoundCircle();
		featureListTab();
		DeadMenuConfig();
		revolutionSliderActiver();
		wowActivator();
		mobileMenuConfig();
		contactFormValidation();

	// window load
	//$(window).on('load', function () {
	//	handlePreloader();
	//});
	// window scroll
	$(window).on('scroll', function () {
		stickyHeader();
		OnePageMenuScroll();
	});



    //createFacebook(document, 'script', 'facebook-jssdk');
    function createFacebook(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.8&appId=186188158140165";
        fjs.parentNode.insertBefore(js, fjs);
    };

};


function getScript(source, callback) {
    var script = document.createElement('script');
    var prior = document.getElementsByTagName('script')[0];
    script.async = 1;
    prior.parentNode.insertBefore(script, prior);

    script.onload = script.onreadystatechange = function (_, isAbort) {
        if (isAbort || !script.readyState || /loaded|complete/.test(script.readyState)) {
            script.onload = script.onreadystatechange = null;
            script = undefined;

            if (!isAbort) { if (callback) callback(); }
        }
    };

    script.src = source;
}

getScript('/js/jquery.min.js',
    function() {
        $.when(
            $.getScript("/js/jquery.cookie.min.js"),
            $.getScript("/js/jquery.themepunch.tools.min.js"),
            $.getScript("/js/jquery.themepunch.revolution.min.js"),
            $.getScript("/js/countdown.min.js"),
            $.getScript("/js/jquery.easing.min.js"),
            $.getScript("/js/jquery.mixitup.min.js"),
            $.getScript("/js/jquery.bxslider.min.js"),
            $.getScript("/js/owl.carousel.min.js"),
            $.getScript("/js/circle-progress.js"),
            $.getScript("/js/jquery.appear.js"),
            $.getScript("/js/jquery.countTo.js"),
            $.getScript("/js/jquery.countTo.js"),
            $.getScript("/js/wow.js"),
            $.getScript("/js/jquery.fancybox.pack.js"),
            $.Deferred(function (deferred) {
                $(deferred.resolve);
            })
        ).done(function() {
            Common();
            initTicketForm();
        });
    });
  