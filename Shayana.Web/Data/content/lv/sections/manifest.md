---
weight: 20
menu-title: Manifests
---
# 
# Manifests

Trešais festivāla gads ir klāt un tā iespējas ir šķietami neierobežotas!
Iepriekšējais gads bija krāsu un emociju sprādziens, un mēs arvien turpinām augt un pilnveidoties. Tiek aicināti visi, kas mīl atpūtu dabā labas mūzikas pavadījumā.

Ar mums kopā sanāk dažādi cilvēki. Tie, kas mīl dejas līdz rīta gaismai, kā arī tie, kam vairāk labpatīk vērot ugunskuru mūzikas pavadījumā. Katrs ir mums svarīgs!

2018.gadā mēs plānojam ieviest jaunas idejas un nomainīt skatuvju konceptu, lai uzdāvinātu apmeklētājiem dzestru brīnuma sajūtu. Mēs pakāpeniski ziņosim par jaunumiem - sekojiet līdzi.


Ja dzidejāt par mums, tātad esiet uzaicināts.

[Šajanas sūtnis Facebook'­ā ](https://www.facebook.com/profile.php?id=100013224242512&fref=ts) –  mūsu draugs no enerģijas un gaismas. Viņš pastāstīs, kā nonākt festivālā, un kā Jūs varat mums palīdzēt.
