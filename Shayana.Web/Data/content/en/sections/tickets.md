---

weight: 25
menu-title: Tickets
---
#
# Tickets

Presale ended! See you at the gate.

Ticket cost at the gate is **25€** for three days, **15€** for two. Cash only!

<!-- Early bird tickets are now available. Presale three-day ticket price is **15€** including camping place. Ticket for group of four people costs only **50€**. -->

<!-- Please keep in mind **presale ends on May 15**! -->

<!-- You can buy ticket by bank transfer using this form. Please fill in this form (as in passport), and we will send you details. -->

<!-- If you are minor, parent must be with you. -->

<h4 class='ticketOrderSuccess' style='display:none'>Your order has been submitted successully. We will contact you soon with details.</h4>
<h4 class='ticketOrderError' style='display:none'>Sorry, failed to submit your order, please write us at pspmagic88@gmail.com to order ticket or try again.</h4>
<div style='display:none' id='ticketForm'></div>


<!-- You can also buy early bird ticket using [<img src='/images/paypal.png' alt='paypal donate' width='100px'/>](https://www.paypal.me/shayanalv/15).   -->
<!-- Please choose "donation", not "services" there and **let us know by e-mail!** -->

<!-- Tickets on gate will be costing **25€**, so take your chance! -->

If you want to book festival shuttle, you can easily do it with [our partner latvia-outdoor.de ![latvia outdoor](/images/latvia-outdoor-logo-128.png "Latvia Oudoor")](http://latvia-outdoor.de/shayanaland.htm)

Two-person tents (w/o sleeping bag!) for rent are available at 3€ per night, but please book at advance.


<!-- Or you can transfer money to: -->
 <!--  -->
<!-- Name: Vladislavs Kugelevics   -->
<!-- IBAN: DE48100110012621223742   -->
<!-- BIC: NTSBDEB1XXX   -->

<!-- Please set topic to "Shayana: your_email_here" or let us know by email [vlad@izvne.com](mailto:vlad@izvne.com). -->

<!-- **Bank Address** -->

<!-- N26 Bank GmbH   -->
<!-- Klosterstraße 62,   -->
<!-- 10179 Berlin   -->
<!-- Registered in Commercial Register Local Court Charlottenburg HRB 170602 B   -->
<!-- VAT-ID: DE305957096   -->

**Volunteering and support**

Our festival is non-commercial, so supporters are welcome. If you think you can spend larger sum, we have special offers for private sponsors, please [contact us](#contact "Contact Us") to discuss this.
