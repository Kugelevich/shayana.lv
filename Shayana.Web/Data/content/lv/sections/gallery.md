﻿weight: 95
menu-title: Gelerija
layout: gallery
title: Galerija
subtitle: Bilders no pre-party, festivāla un kupola celšanas
multi:
 - Preparty 2017
 - Shayana 2017 - DJs
 - Shayana 2017 - staff
 - Shayana 2017 - visitors
 - Shayana 2017 - decor
 - Ligo 2017
---
Klikšķiniet uz bildes un izmantojie bultas vai peli lai apskatītos visas bildes galerijā

<!-- Author of photos - [Sandra The Lizard](https://www.facebook.com/jascherica) -->
