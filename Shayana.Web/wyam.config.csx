#n -p Wyam.Html 
#n -p Wyam.Less
#n -p Wyam.Markdown
#n -p Wyam.Minification
#n -p Wyam.Razor
#n -p Wyam.Yaml
//#a obj/**

#a "System.Collections, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
#a "System.ServiceModel, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"
#a "System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"

using System.Text.RegularExpressions;

public string GetLanguage (string path) {
    return path.Substring (path.IndexOf ("content/") + 8, 2);
}
FileSystem.InputPaths.Add ("Data/");
FileSystem.OutputPath = "wwwroot/";
    Settings [Keys.Host] = "shayana.lv";
Settings [Keys.LinksUseHttps] = true;


Pipelines.Add ("Assets",
 CopyFiles ("**/*.{png,gif,jpg,woff,woff2,svg,ico,txt,pdf,html}")
);


Pipelines.Add ("Gallery",
 ReadFiles ("images/**/*.jpg")
);


Pipelines.Add ("Js",
 ReadFiles ("js/*.js"),
 MinifyJs (),
 WriteFiles ("js/")
);



Pipelines.Add ("CssLang",
ReadFiles ("css/**/*.css"),
   MinifyCss (),
   WriteFiles ("css/")
);



Pipelines.Add ("Lineup",
            ReadFiles ("content/*/lineup/**/*.md"),
            FrontMatter (Yaml ()),
            Meta ("lang", GetLanguage (@doc ["SourceFileDir"].ToString ())),
            Meta ("pic", @doc ["SourceFileBase"]),
            Markdown (),  // Convert it to HTML         
            Razor ().WithLayout ("/_layouts/performer.cshtml"),
            Meta ("Content", @doc.Content),
            OrderBy (@doc ["DateTime"].ToString ().Split (':') [0].Trim().Length).Descending ().ThenBy (@doc ["DateTime"])

);

Pipelines.Add ("Sections",
            ReadFiles ("content/*/sections/*.md"),
            FrontMatter (Yaml ()),  // Move the frontmatter to metadata
            Meta ("lang", GetLanguage (@doc ["SourceFileDir"].ToString ())),
            Markdown (),  // Convert it to HTML         
            Razor ().WithViewStart ("_layouts/_ViewStartSection.cshtml"),
            Meta ("Content",@doc.Content),            
            OrderBy (int.Parse (@doc ["weight"].ToString ())) // Order for consistency  
);



Pipelines.Add ("Index",
        ReadFiles ("content/*/index.cshtml"),
        FrontMatter (Yaml ()),
        Meta ("lang", GetLanguage (@doc ["SourceFileDir"].ToString ())),
        Meta ("pathLang", @doc ["lang"].ToString ().ToLower () == "en" ? "" : @doc ["lang"] + "/"),
        Meta ("canonical", "/" + @doc ["pathLang"]),     
        Razor ().WithViewStart ("_layouts/_ViewStart.cshtml"),
        MinifyHtml (),
        WriteFiles (@doc ["pathLang"].ToString () + @doc ["WritePath"].ToString ()).UseWriteMetadata (false)
);
