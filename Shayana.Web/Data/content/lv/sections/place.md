---
menu-title: Vieta
weight: 40
---
#
# VIETA
Festivāla norises vieta atrodās pasakaini skaistajā dabas liegumā "Daugavas loki". Daugava atrodās tikai 15 minūšu gājiena attālumā. 
Maģiski kalnu pauguri, purvs, bērzu birzis, senas akmens celtnes satinušās vīteņaugos gaida tieši Tevi- Šajaniet!
Festivāla teritorijas tuvumā nav veikalu, tāpēc pārliecinies, ka viss tev nepieciešamais ēdiens un dzeramais ūdens ir sagādāts iepriekš! Mēs centīsimies nodrošināt dzeramā ūdens esamību, taču tā daudzums ir ierobežots.

## Kā lai tiek līdz turienei?

* Ir pieejams transfērs no Rīgas, lidostas un centrālās autobusu stacijas (Rigas Autoosta).
Ja vēlaties rezervēt festivāla transfēru, jūs to varat izdarīt ar 
    [mūsu parneri latvia-outdoor.de](http://latvia-outdoor.de/shayanaland.htm)



* Sabiedriskā transporta piejamība šajā apvidū ir ierobežota. Autobusu pieturs ir "Orehovka".

    [Autobusu saraksti](https://www.1188.lv/satiksme/saraksti/daugavpils-ao/malkalni/105258/103409/diena/2018-05-25)
    
     

*   Festivāla Koordinātes : **26°50'57.10"E 55°52'33.10"N**  vai  **55.875893, 26.849181**

    [Saite Google Maps maršutam](https://www.google.lv/maps/dir/%D0%94%D0%B0%D1%83%D0%B3%D0%B0%D0%B2%D0%BF%D0%B8%D0%BB%D1%81,+Daugavpils+pils%C4%93ta/55.8758611,26.8491944/@55.8717293,26.6104813,20540m/data=!3m2!1e3!4b1!4m8!4m7!1m5!1m1!1s0x46c29430ff01c9db:0x400cfcd68f307f0!2m2!1d26.536179!2d55.874736!1m0)

<iframe src="https://www.google.com/maps/embed?pb=!1m26!1m12!1m3!1d112963.13184779328!2d26.54043883133212!3d55.871573320566974!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m11!3e6!4m5!1s0x46c29430ff01c9db%3A0x400cfcd68f307f0!2z0JTQsNGD0LPQsNCy0L_QuNC70YEsIERhdWdhdnBpbHMgcGlsc8STdGE!3m2!1d55.874736!2d26.536178999999997!4m3!3m2!1d55.875861099999995!2d26.8491944!5e0!3m2!1sen!2slv!4v1495179725545"  frameborder="0" style="width:80%;min-height:400px;border:0" allowfullscreen></iframe>




*   Pateicoties maršrutam kartē, mūs ir viegli atrast pat bez GPS ierīces, jo Daugavpili un Šajānalendu atdala tikai divi pagriezieni.

Maršruta apraksts: 

Izbrauciet no Daugavpils, braucot pa ceļu P68 caur Grivas mikrorajonu, Baltkrievijas robežas virzienā;

Pirmajā krustojumā, kas ir uzreiz pēc ceļazīmes ar nosaukumu “Daugavpils pilsētas beigas”, brauciet pa kreisi

[Pic1](/images/location/1.jpg "Pic1")

Dodieties šajā virzienā tik tālu, līdz ieraudzīsiet lielo krustojumu ar baznīcu, turpiniet ceļu līdz sāksies gruntceļš;

[Pic2](/images/location/2.jpg "Pic2")

Turpiniet maršrutu, braucot pa gruntceļu un šķērsojot krustojumu

[Pic3](/images/location/3.jpg "Pic3")

Tiklīdz jūs ieraudzīsiet elektrības pārvadu līniju - tas ir Šajānalendas sākums!

[Pic4](/images/location/4.jpg "Pic4")

Viss ceļš līdz festivālam ir aptuveni 28 kilometrus garš, ja dosieties no Daugavpils centra - ceļš aizņems 30 minūtes. Vadītāji, lūdzu, novietojiet savas automašīnas tā, lai nevienam netraucētu - gan citiem festivāla dalībniekiem, gan apkārtesošajiem zemniekiem.

Esat laipni gaidīti!





## Nakšņošanas vietas?

Tā nav vienkārša sakritība, ka Shayana notiek maijā, zem klajas debess. Mēs esam pārliecināti, ka māte Daba atļaus jums šajā laikā atgūt spēkus un relaksēties teltīs vai starp kokiem iekārtos šūpultīklos - tos gan jums vajadzēs sarūpēt pašiem. Parūpējieties par visu nepieciešamo jau iepriekš noteikti neaizmirstot paņemt lieku zeķu pāri un guļammaisu un visu pārējo komfortablai nakšņošanai dabā.
Ja jūs neesiet pieraduši pie nakšņošanas dabā varat lūgt padomu, kā arī noīrēt kvalitatīvu tūrisma ekipējumu pie mūsu draugiem - [tūrisma klubs "Sniegpulkstenīte"](http://www.sniegpulkstenite.lv/) (Daugavpils) 

## Vai maija beigās vēl nav par aukstu lai nakšņotu brīvā dabā?

Statistika vēsta - ir mazticams tas, ka gaisa temperatūra naktīs būtu zemāka par +6’C. Obligāti ģērbieties atbilstoši laka apstākļiem un tam, ka nakšosiet ārā.
Mēs darīsim visu iespējamo lai sasildītu jūs ar pozitīvu enerģiju, teritorijā tam paredzētās vietās tiks ierīkoti ugunskuri, pie kuriem noteikti varēs sasildīties.
Taču pati pārbaudītākā Shayana metode lai sasildītos ir doties uz Energy Field un no sirds izdejoties!

#### 
