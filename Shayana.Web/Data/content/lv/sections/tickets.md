---

weight: 25
menu-title: Biļetes
---
#
# Biļetes

Iepriekšpārdošanā ir beigusies.

Festivāla dienās biļetes būs pieejam uz vietas par **25€**. Cash only!
 <!--  -->
 <!-- pieejamas biļetes par īpašu cenu - **15€** par visām trīm festivāla dienām, iekļaujot vietu teltīm. Biļetes cena grupai no četriem cilvēkiem maksā tikai **50€**. -->

<!-- Please keep in mind **presale ends on May 15**! -->

<!-- Jūs varat iegādāties biļeti ar bankas pārskaitījumu izmantojot sekojošo formu. Lūdzam aizpildīt šo formu (kā pasē) un mēs Jums izsūtīsim maksājuma detaļas. -->

<!-- Līdz 18 gadiem ieeja uz festivālu tikai ar vecākiem.  -->

<!-- <h4 class='ticketOrderSuccess' style='display:none'>Jūsu pasūtījums ir veiksmīgi pieņemts. Mēs ar jums sazināsimies 24 studnu laikā.</h4> -->
<!-- <h4 class='ticketOrderError' style='display:none'>Mums neizdevās pieņmet Jūsu pasūtījumu. Lūdzam sazināties ar mums rakstos uz pspmagic88@gmail.com, vai mēģiniet vēlreiz.</h4> -->
<!-- <div id='ticketForm'></div> -->


<!-- Biļetes iepriekšpārdošanā iespējams iegādāties, izmantojot [PayPal](https://www.paypal.me/shayanalv/15). -->
<!-- Veicot maksājumu lūdzam izvēlēties "donation", nevis "services". -->

<!-- Festivāla dienās biļetes būs pieejam uz vietas par **25€**, nepalaid garām iespēju! -->

Ja vēlaties rezervēt festivāla transfēru, jūs to varat izdarīt ar [mūsu parneri latvia-outdoor.de ![latvia outdoor](/images/latvia-outdoor-logo-128.png "Latvia Oudoor")](http://latvia-outdoor.de/shayanaland.htm)

Ir iespējams noīrēt divvietīgas teltis (bez guļammaisa!) - cena par vienu nakti 3€. Lūdzam veikt rezervāciju laicīgi.

**Atbalsts un brīvprātīgais darbs**

Mūsu festivāls nav komerciāls pasākums, tāpēc būsim priecīgi saņemt jebkāda veida atbalstu. Ja jūtat, ka vēlaties palīdzēt vai varat ziedot kādu lielāku naudas summu, mums ir īpašs piedāvājums šādiem gadījumiem. Lūdzam [sazināties](#contact "Sazināties"), lai to apspriestu.
