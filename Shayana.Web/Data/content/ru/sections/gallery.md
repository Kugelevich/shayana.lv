﻿weight: 95
menu-title: Галерея
layout: gallery
title: Галерея
subtitle: Галереи с пре-пати, феста и стройки геокупола
multi:
 - Preparty 2017
 - Shayana 2017 - DJs
 - Shayana 2017 - staff
 - Shayana 2017 - visitors
 - Shayana 2017 - decor
 - Ligo 2017
---
Нажмите на фото и используйте стрелки или мышь, чтобы листать альбом.

<!-- Автор фото - [Сандра Ящерица](https://www.facebook.com/jascherica) -->

