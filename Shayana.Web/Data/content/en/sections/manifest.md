---
weight: 20
menu-title: Manifest
---
# 
# Manifest

Third year of Shayana is coming and possibilities are endless!
Last year was a blast, and we continue to grow and evolve. People that love incredible cocktail of good music and breathing of the night forest are welcome!

We have different people celebrating with us. Those, who love all night long dancing, and those, who value deep and aware music listening near the campfire while drinking aged pu-erh. Everybody is important to us!

In year 2018, we plan to introduce a fresh wave into festival, and change stages concept to bring visitors new sense of wonder. We'll announce our plans gradually, so bear with us.  


If you heard about us, you are already invited!

[Shayana Ambassador on Facebook](https://www.facebook.com/profile.php?id=100013224242512&fref=ts) is our friend made of Energy And Light. He will whisper to you, how you can get to the festival, how you can participate and how to help!
